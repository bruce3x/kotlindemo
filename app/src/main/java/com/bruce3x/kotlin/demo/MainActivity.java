package com.bruce3x.kotlin.demo;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private FoodOrderService service = new FoodOrderService(true);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {
        RadioGroup options = findViewById(R.id.rg_options);
        View btnSelect = findViewById(R.id.btn_select);
        View btnReset = findViewById(R.id.btn_reset);

        TextView tvDetail = findViewById(R.id.tv_detail);
        tvDetail.setText(service.getOrderDetail());

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (options.getCheckedRadioButtonId()) {
                    case R.id.rb_food_a:
                        select("A");
                        break;
                    case R.id.rb_food_b:
                        select("B");
                        break;
                    case R.id.rb_food_c:
                        select("C");
                        break;
                    default:
                        break;
                }
            }

            private void select(String food) {
                try {
                    service.select(food);
                    tvDetail.setText(service.getOrderDetail());
                } catch (IllegalArgumentException e) {
                    Toast.makeText(MainActivity.this, "select error", Toast.LENGTH_SHORT).show();
                } finally {
                    // no-op
                }
            }
        });

        btnReset.setOnClickListener(v -> {
            service.reset();
            tvDetail.setText(service.getOrderDetail());
        });
    }
}
