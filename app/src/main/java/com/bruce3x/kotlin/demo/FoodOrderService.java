package com.bruce3x.kotlin.demo;

import java.util.ArrayList;
import java.util.List;

public class FoodOrderService {
    private List<Food> items = new ArrayList<>();
    private boolean isVip;

    public FoodOrderService(boolean isVip) {
        this.isVip = isVip;
    }

    public void select(String food) {

        switch (food) {
            case "A":
                items.add(new Food(food, 1.5F));
                break;
            case "B":
                items.add(new Food(food, 3.5F));
                break;
            case "C":
                items.add(new Food(food, 5F));
                break;
            default:
                throw new IllegalArgumentException("Unsupported food.");
        }

    }

    public String getOrderDetail() {

        StringBuilder builder = new StringBuilder();
        float total = 0;
        for (Food item : items) {
            builder
                    .append("Food: ").append(item.name)
                    .append("    ")
                    .append("Price: ").append("¥").append(item.price)
                    .append("\n");
            total += item.price;
        }

        float amount;
        if (isVip) {
            // 88折哟
            amount = 0.88F * total;
        } else {
            amount = total;
        }

        return String.format("Welcome!!!\n\n%s\n\nTotal: %.2f", builder.toString(), amount);
    }

    public void reset() {
        items.clear();
    }

    private static class Food {
        public String name;
        public float price;

        public Food(String name, float price) {
            this.name = name;
            this.price = price;
        }
    }
}
